import React from "react";
import { render } from "react-dom";
import { App } from "./components/App";
import "./root.css";

render(<App />, document.getElementById("root"));
