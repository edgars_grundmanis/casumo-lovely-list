const SERVER_URL = "http://localhost:80";

export const endpoint = {
    BOOKS: `${SERVER_URL}/books`,
};

// Let's just assume server returned that at some point
export const BOOKS_QUANTITY = 1e6;
