import React, { Component, ReactElement } from "react";
import { AutoSizer, Index, IndexRange, InfiniteLoader, List, ListRowProps, Size } from "react-virtualized";
import { Book } from "../../types";
import { fetchBooks } from "../../helpers/fetchBooks";
import { BOOKS_QUANTITY } from "../../constants";
import styles from "./App.module.css";
import { BookRow } from "../BookRow/BookRow";

const ROW_HEIGHT = 60;
const MIN_BATCH_SIZE = 50;

type IsRowLoaded = (params: Index) => boolean;
type RenderRow = (props: ListRowProps) => ReactElement;
type LoadMoreRows = (range: IndexRange) => Promise<void>;

interface AppState {
    books: Map<number, Book>;
}

export class App extends Component<{}, AppState> {
    state = {
        books: new Map(),
    };

    private isRowLoaded: IsRowLoaded = ({ index }) => this.state.books.has(index);

    private rowRenderer: RenderRow = ({ index, key, style }) => (
        <BookRow
            style={style}
            key={key}
            bookData={this.state.books.get(index)}
        />
    );

    private loadMoreRows: LoadMoreRows = ({ startIndex, stopIndex }) => {
        // stopIndex should be incremented by one, as the response would be exclusive "to" item.
        return fetchBooks({ from: startIndex, to: stopIndex + 1 })
            .then((books) => { this.setFetchedBooks(books, startIndex); });
    };

    /**
     * Adds fetched books to books Map using range index as their key,
     * that allows getting them by row index in rowRenderer.
     */
    private setFetchedBooks = (books: Book[], startIndex: number) => {
        this.setState((prevState) => {
            // As a possible future optimiziation:
            // it should be more performant to mutate map, instead of recreating each time.
            const allBooks = new Map(prevState.books);

            // Use for loop instead of forEach as we are on a hot path.
            for (let i = 0; i < books.length; i++) {
                allBooks.set(startIndex + i, books[i]);
            }

            return { ...prevState, books: allBooks };
        });
    };

    render() {
        return (
            <div className={styles.container}>
                <InfiniteLoader
                    isRowLoaded={this.isRowLoaded}
                    loadMoreRows={this.loadMoreRows}
                    rowCount={BOOKS_QUANTITY}
                    minimumBatchSize={MIN_BATCH_SIZE}
                >
                    {({ onRowsRendered, registerChild }) => (
                        <AutoSizer>
                            {({ width, height }: Size) => (
                                <List
                                    ref={registerChild}
                                    height={height}
                                    onRowsRendered={onRowsRendered}
                                    rowCount={BOOKS_QUANTITY}
                                    rowHeight={ROW_HEIGHT}
                                    rowRenderer={this.rowRenderer}
                                    width={width}
                                />
                            )}
                        </AutoSizer>
                    )}
                </InfiniteLoader>
            </div>
        );
    }
}
