import React from "react";
import { shallow } from "enzyme";
import { BookItem } from "./BookItem";
import { Genre } from "../../../../types";

describe("BookItem", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<BookItem
            bookName="Batman"
            authorName="Bruce Wayne"
            genre={Genre.Blockchain}
            publishDate={1579634074327}
        />);
        expect(wrapper.exists()).toBe(true);
    });

    it("should match snapshot", () => {
        const wrapper = shallow(<BookItem
            bookName="Batman"
            authorName="Bruce Wayne"
            genre={Genre.Blockchain}
            publishDate={1579634074327}
        />);
        expect(wrapper).toMatchSnapshot();
    });
});
