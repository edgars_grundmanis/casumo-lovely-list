import React, { FC } from "react";
import { Genre } from "../../../../types";
import { BookItemContainer } from "../BookItemContainer";

interface BookItemProps {
    bookName: string;
    authorName: string;
    genre: Genre;
    publishDate: number;
}

function formatPublishDate(timestamp: number) {
    return new Date(timestamp).toLocaleDateString();
}

export const BookItem: FC<BookItemProps> = ({
    bookName,
    authorName,
    genre,
    publishDate,
}) => (
    <BookItemContainer
        bookName={bookName}
        authorName={` by ${authorName}`}
        genre={genre}
        publishDate={formatPublishDate(publishDate)}
    />
);
