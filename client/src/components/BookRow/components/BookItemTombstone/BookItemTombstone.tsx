import React, { FC } from "react";
import { Tombstone } from "../Tombstone/Tombstone";
import { BookItemContainer } from "../BookItemContainer";

export const BookItemTombstone: FC = () => (
    <BookItemContainer
        bookName={<Tombstone chars={40} />}
        authorName=""
        genre={<Tombstone chars={10} />}
        publishDate={<Tombstone chars={9} />}
    />
);
