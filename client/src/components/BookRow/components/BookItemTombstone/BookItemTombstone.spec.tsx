import React from "react";
import { shallow } from "enzyme";
import { BookItemTombstone } from "./BookItemTombstone";

describe("BookItemTombstone", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<BookItemTombstone />);
        expect(wrapper.exists()).toBe(true);
    });

    it("should match snapshot", () => {
        const wrapper = shallow(<BookItemTombstone />);
        expect(wrapper).toMatchSnapshot();
    });
});
