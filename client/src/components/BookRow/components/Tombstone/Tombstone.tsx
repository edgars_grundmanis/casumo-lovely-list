import React, { FC } from "react";

export interface TombstoneProps {
    chars: number;
}

export const Tombstone: FC<TombstoneProps> = ({ chars }) => (
    <span
        style={{
            background: "#efefef",
            width: `${chars}ch`,
            display: "inline-block",
        }}
    >
        {/* White space is needed to have an intrinsic height for the element. */}
        &nbsp;
    </span>
);
