declare const styles: {
  readonly "bookItem": string;
  readonly "author": string;
  readonly "bookName": string;
  readonly "genrePublishDateRow": string;
};
export = styles;

