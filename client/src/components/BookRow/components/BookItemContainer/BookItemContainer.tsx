import React, { FC, ReactNode } from "react";
import styles from "./BookItemContainer.module.css";

interface BookItemContainerProps {
    bookName: ReactNode;
    authorName: ReactNode;
    genre: ReactNode;
    publishDate: ReactNode;
}

export const BookItemContainer: FC<BookItemContainerProps> = ({
    bookName,
    authorName,
    genre,
    publishDate,
}) => (
    <div className={styles.bookItem}>
        <p>
            <span className={styles.bookName}>{bookName}</span>
            <span className={styles.author}>{authorName}</span>
        </p>
        <p className={styles.genrePublishDateRow}>
            <span>{genre}</span>
            <span>{publishDate}</span>
        </p>
    </div>
);
