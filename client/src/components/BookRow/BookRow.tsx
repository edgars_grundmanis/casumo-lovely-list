import React, { FC, CSSProperties } from "react";
import { Book } from "../../types";
import styles from "./BookRow.module.css";
import { BookItem } from "./components/BookItem";
import { BookItemTombstone } from "./components/BookItemTombstone";

interface RowProps {
    style: CSSProperties;
    bookData?: Book;
}

// Reuse same component for all rows.
const BookTombstone = <BookItemTombstone />;

export const BookRow: FC<RowProps> = ({ style, bookData }) => {
    const component = bookData
                      ? (
                          <BookItem
                              bookName={bookData.name}
                              authorName={bookData.author.name}
                              genre={bookData.genre}
                              publishDate={bookData.publishDate}
                          />
                      )
                      : BookTombstone;
    return (
        <div style={style} className={styles.row}>
            {component}
        </div>
    );
};
