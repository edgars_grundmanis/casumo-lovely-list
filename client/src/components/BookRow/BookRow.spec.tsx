import React from "react";
import { shallow } from "enzyme";
import { BookRow } from "./BookRow";
import { Gender, Genre } from "../../types";
import { BookItemTombstone } from "./components/BookItemTombstone";
import { BookItem } from "./components/BookItem";

const style = { width: 100 };
const bookData = {
    id: "1-1",
    name: "Batman",
    author: {
        name: "Bruce Wayne",
        gender: Gender.Male,
    },
    genre: Genre.Programming,
    publishDate: 1579634074327,
};

describe("BookRow", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<BookRow
            style={style}
            bookData={bookData}
        />);
        expect(wrapper.exists()).toBe(true);
    });

    it("should properly set style", () => {
        const wrapper = shallow(<BookRow
            style={style}
            bookData={undefined}
        />);
        const expected = style;
        const actual = wrapper.first().prop("style");
        expect(actual).toBe(expected);
    });

    it("should render BookItemTombstone when book data is *NOT* present", () => {
        const wrapper = shallow(<BookRow
            style={style}
            bookData={undefined}
        />);
        const expected = true;
        const actual = wrapper.contains(<BookItemTombstone />);
        expect(actual).toBe(expected);
    });

    it("should render BookItem when book data is present", () => {
        const wrapper = shallow(<BookRow
            style={style}
            bookData={bookData}
        />);
        const expected = true;
        const actual = wrapper.contains(
            <BookItem
                bookName={bookData.name}
                authorName={bookData.author.name}
                genre={bookData.genre}
                publishDate={bookData.publishDate}
            />,
        );
        expect(actual).toBe(expected);
    });

    it("should match snapshot", () => {
        const wrapper = shallow(<BookRow
            style={style}
            bookData={bookData}
        />);
        expect(wrapper).toMatchSnapshot();
    });
});
