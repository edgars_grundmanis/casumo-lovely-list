import { endpoint } from "../constants";
import { Book } from "../types";

export type Range = { from: number; to: number; };

export function fetchBooks({ from, to }: Range): Promise<Book[]> {
    return window.fetch(`${endpoint.BOOKS}?from=${from}&to=${to}`)
                 .then((response) => {
                     if (!response.ok) {
                         return response.text()
                                        .then((text) => Promise.reject(text));
                     }

                     return response.json();
                 })
                 .catch((error: string) => {
                     window.console.error(error);
                     throw Error(error);
                 });
}
