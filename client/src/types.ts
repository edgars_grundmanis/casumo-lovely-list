export enum Genre {
    Horror = "horror",
    Finance = "finance",
    Blockchain = "blockchain",
    Programming = "programming",
}

export enum Gender {
    Male = "male",
    Female = "female",
}

export interface Author {
    name: string;
    gender: Gender;
}

export interface Book {
    id: string;
    name: string;
    author: Author;
    genre: Genre;
    publishDate: number;
}
