exports.genre = {
    horror: "horror",
    finance: "finance",
    blockchain: "blockchain",
    programming: "programming",
};

exports.gender = {
    male: "male",
    female: "female",
};
