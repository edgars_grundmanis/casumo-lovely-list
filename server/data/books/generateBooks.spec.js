const generateBooks = require("./generateBooks");

jest.setTimeout(30000);

describe("generateBooks", () => {
    it("should generate random books", () => {
        const expected = (
            Array(2)
                .fill(0)
                .map(() => ({
                    id: expect.any(String),
                    author: {
                        name: expect.any(String),
                        gender: expect.any(String),
                    },
                    name: expect.any(String),
                    genre: expect.any(String),
                    publishDate: expect.any(Number),
                }))
        );

        return generateBooks(2)
            .then((actual) => {
                expect(actual).toEqual(expected);
            });
    });

    it("should not block main thread longer than for ~ 20ms", () => {
        const nsToMs = (ns) => (ns / 1e6) | 0;
        const expectToBeInSecondDozen = (n) => expect(n / 100).toBeCloseTo(0.2, 1);
        const booksQuantity = 1e4;

        // Measure thread block time
        const startTime_1 = process.hrtime();
        const books = generateBooks(booksQuantity);
        const [secondsBlocked_1, nsBlocked_1] = process.hrtime(startTime_1);
        expect(secondsBlocked_1).toBe(0);
        // It's flaky to measure elapsed milliseconds due to variable environments and perfs.
        // Verify that a thread is at least not blocked for more than two dozen ms.
        expectToBeInSecondDozen(nsToMs(nsBlocked_1));

        // Measure thread block time once more, to be sure we're no blocking something again
        const startTime_2 = process.hrtime();
        setImmediate(() => {
            const [secondsBlocked_2, nsBlocked_2] = process.hrtime(startTime_2);
            expect(secondsBlocked_2).toBe(0);
            // It's flaky to measure elapsed milliseconds due to variable environments and perfs.
            // Verify that a thread is at least not blocked for more than two dozen ms.
            expectToBeInSecondDozen(nsToMs(nsBlocked_2));
        });

        return books.then((array) => {
            expect(array.length).toBe(booksQuantity);
        });
    });
});
