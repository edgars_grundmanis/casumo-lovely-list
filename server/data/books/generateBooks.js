const uuid = require("uuid/v4");
const { generateRandomAuthor } = require("./helpers/generateRandomAuthor");
const { generateRandomBookName } = require("./helpers/generateRandomBookName");
const { getRandomItem, getRandomDate } = require("../../utils");
const { genre } = require("./constants");

/**
 * This function will be executed on a hot path, it's necessary to preemptively calculate it's operands.
 */
const createRandomBook = (genres) => ({
    id: uuid(),
    author: generateRandomAuthor(),
    name: generateRandomBookName(),
    genre: genre[getRandomItem(genres)],
    publishDate: getRandomDate().getTime(),
});

//
/**
 * Generates books *asynchronously* without blocking main thread (using batching approach).
 * Readability is sacrificed in favor of performance.
 * Benchmarks: https://jsperf.com/property-lookup-price
 */
function generateBooks(n) {
    /* Microoptimizations. */
    // Precreate array with empty slots
    this.books = new Array(n);
    // Cycle reference local properties, otherwise we have no guarantees
    // that it will not do a property lookup on every iteration.
    const books = this.books;
    // Precalculate beforehand once.
    const genres = Object.keys(genre);

    return new Promise((resolve, _) => {
        let bookIdx = 0;

        // Bataching approach. Segment larger task into micro-tasks.
        createBooks();

        function createBooks(taskStartTime = process.hrtime()) {
            let taskElapsedTimeNs;

            do {
                // Create a book
                // Direct assigment is more performant than push.
                books[bookIdx++] = createRandomBook(genres);

                // Go again if there’s enough time to create a next book.
                taskElapsedTimeNs = process.hrtime(taskStartTime)[1];
            } while (
                taskElapsedTimeNs / 1e6 /* convert ns to ms */ < 20
                && bookIdx < n
            );

            if (bookIdx < n) {
                setImmediate(createBooks);
            } else {
                resolve(books);
            }
        }
    });
}

module.exports = generateBooks;
