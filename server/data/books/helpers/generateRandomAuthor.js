const { getRandomItem } = require("../../../utils");
const { gender } = require("../constants");

const MALE_AUTHOR_NAMES = ["Edgar", "Michele", "Andrew", "Bruce"];
const MALE_AUTHOR_SURNAMES = ["Grundmanis", "Brown", "Wayne", "Parker"];
const FEMALE_AUTHOR_NAMES = ["Kate", "Cassandra", "Selina", "Emma"];
const FEMALE_AUTHOR_SURNAMES = ["Casumo", "Cain", "Kyle", "Bertinelli"];

/* Microoptimizations */
// Shortcircuit property lookups.
const male = gender.male;
const female = gender.female;

/**
 * This function is expected to be executed on a hot path, it's necessary to apply microoptimizations.
 */
exports.generateRandomAuthor = function generateRandomAuthor() {
    const randomGender = Math.random() > 0.5 ? male : female;
    const nameVariants = randomGender === female ? FEMALE_AUTHOR_NAMES : MALE_AUTHOR_NAMES;
    const surnameVariants = randomGender === male ? FEMALE_AUTHOR_SURNAMES : MALE_AUTHOR_SURNAMES;

    return {
        name: `${getRandomItem(nameVariants)} ${getRandomItem(surnameVariants)}`,
        gender: randomGender,
    };
};
