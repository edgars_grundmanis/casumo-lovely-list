let mockRandomIndex = 0;
jest.mock("../../../utils/getRandomItem", () => ({
    getRandomItem: (arr) => arr[mockRandomIndex],
}));

const { generateRandomAuthor } = require("./generateRandomAuthor");

describe("generateRandomAuthor", () => {
    it("should return author of the correct shape", () => {
        const expected = { name: expect.any(String), gender: expect.any(String) };
        const actual = generateRandomAuthor();
        expect(actual).toEqual(expect.objectContaining(expected));
    });

    it("should generate random author", () => {
        mockRandomIndex = 0;
        const actual1 = generateRandomAuthor();

        mockRandomIndex = 1;
        const actual2 = generateRandomAuthor();

        expect(actual1).not.toEqual(actual2);
    });
});
