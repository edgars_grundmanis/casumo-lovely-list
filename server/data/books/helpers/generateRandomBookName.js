const { getRandomItem } = require("../../../utils");

const BOOK_NAME_COMPOUNDS = {
    adjective: [
        "aggressive",
        "agreeable",
        "ambitious",
        "brave",
        "calm",
        "delightful",
        "eager",
        "faithful",
    ],
    nounPrefix: [
        "history",
        "book",
        "match",
        "cat",
        "sock",
        "ship",
        "hero",
        "baby",
        "area",
        "book",
        "business",
        "case",
        "child",
        "company",
        "country",
        "day",
        "eye",
        "fact",
        "family",
        "government",
        "group",
        "hand",
        "home",
        "job",
        "life",
        "lot",
    ],
    nounPostfix: [
        "man",
        "money",
        "month",
        "mother",
        "Mister",
        "night",
        "number",
        "part",
        "people",
        "place",
        "point",
        "problem",
        "program",
        "question",
        "right",
        "room",
        "school",
        "state",
        "story",
        "student",
        "study",
        "system",
        "thing",
        "time",
        "water",
        "way",
        "week",
        "woman",
        "word",
        "work",
        "world",
        "year",
    ],
};

/* Microoptimizations */
// Shortcircuit local property lookups
const bookAdjective = BOOK_NAME_COMPOUNDS.adjective;
const bookNounPrefix = BOOK_NAME_COMPOUNDS.nounPrefix;
const bookNounPostfix = BOOK_NAME_COMPOUNDS.nounPostfix;

/**
 * This function is expected to be executed on a hot path, it's necessary to apply microoptimizations.
 */
exports.generateRandomBookName = function generateRandomBookName() {
    return `The ${getRandomItem(bookAdjective)} ${getRandomItem(bookNounPrefix)} of ${getRandomItem(bookNounPostfix)}`;
};
