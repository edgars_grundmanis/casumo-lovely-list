let mockRandomIndex = 0;
jest.mock("../../../utils/getRandomItem", () => ({
    getRandomItem: (arr) => arr[mockRandomIndex],
}));

const { generateRandomBookName } = require("./generateRandomBookName");

describe("generateRandomBookName", () => {
    it("should return a string", () => {
        const expected = expect.any(String);
        const actual = generateRandomBookName();
        expect(actual).toEqual(expected);
    });

    it("should generate random book name", () => {
        mockRandomIndex = 0;
        const actual1 = generateRandomBookName();

        mockRandomIndex = 1;
        const actual2 = generateRandomBookName();

        expect(actual1).not.toEqual(actual2);
    });
});
