const { getRandomDate } = require("./getRandomDate");
const { getRandomItem } = require("./getRandomItem");

module.exports = {
    getRandomDate,
    getRandomItem,
};
