const { getRandomDate } = require("./getRandomDate");

describe("getRandomDate", () => {
    it("should return date instance", () => {
        const actual = getRandomDate();
        expect(actual).toBeInstanceOf(Date);
    });

    it("should return random date", () => {
        const actual1 = getRandomDate().getTime();
        const actual2 = getRandomDate().getTime();
        expect(actual1).not.toEqual(actual2);
    });
});
