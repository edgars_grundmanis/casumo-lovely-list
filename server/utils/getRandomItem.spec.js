const { getRandomItem } = require("./getRandomItem");

describe("getRandomItem", () => {
    it("should return random item from array", () => {
        const array = Array(1000).fill(0).map((_, i) => i);
        const actual1 = getRandomItem(array);
        const actual2 = getRandomItem(array);
        expect(actual1).not.toEqual(actual2);
    });
});
