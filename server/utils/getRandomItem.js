/**
 * This function is expected to be executed on a hot path, it's necessary to apply microoptimizations.
 */
exports.getRandomItem = function getRandomItem(array) {
    return array[Math.floor(Math.random() * array.length)];
};
