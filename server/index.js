const express = require("express");
const cors = require("cors");
const generateBooks = require("./data/books");
const app = express();
const port = 80;

const books = generateBooks(1e6);

app.use(cors({ origin: "http://localhost:3000" }));

/**
 * Returns range of books from the list.
 * @method: GET
 * @param {from, to} range of books to return; from - inclusively, to - exclusively
 */
app.get("/books", (req, res) => {
    const from = parseInt(req.query.from, 10);
    const to = parseInt(req.query.to, 10);

    if (Number.isNaN(from) || Number.isNaN(to)) {
        res.status(400)
           .send("Please specify 'from' and 'to' query.");
        return;
    }

    books.then((generatedBooks) => {
             res.send(JSON.stringify(generatedBooks.slice(from, to)));
         })
         .catch((error) => {
             res.status(500)
                .send(error.message);
         });
});

app.listen(port, () => {
    console.log("CORS-enabled web server listening on port 80");
});
